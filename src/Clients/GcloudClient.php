<?php

namespace Storage\Clients;

use Google\Cloud\Storage\StorageClient;
use League\Flysystem\UnableToReadFile;

class GcloudClient
{

    public static function generate(?string $keyfile = null): StorageClient
    {

        $keyfile ??= getenv('GCLOUD_KEYFILE') ?: 'not-configured-keyfile-gcloud';

        if (!file_exists($keyfile)) {
            throw new UnableToReadFile('Keyfile not found: ' . $keyfile);
        }

        return new StorageClient([
            'keyFilePath' => $keyfile
        ]);
    }
}
