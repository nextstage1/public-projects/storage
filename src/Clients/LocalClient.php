<?php

namespace Storage\Clients;

use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToMountFilesystem;
use League\Flysystem\UnableToWriteFile;

class LocalClient
{

    public static function generate(?string $directory = null): string
    {
        $directory ??= getenv('STORAGE_ROOT') ?: null;

        if (null === $directory) {
            throw new UnableToMountFilesystem('Directory not configured. Set env STORAGE_ROOT with the path');
        }

        return $directory;
    }
}
