<?php

namespace Storage\Clients;

use Aws\S3\S3Client as S3ClientAWS;

class S3Client
{

    public static function generate(string $key = '', string $secret = '', string $region = 'us-east-2', string $version = '2006-03-01'): S3ClientAWS
    {
        return new S3ClientAWS([
            'credentials' => [
                'key' => getenv('AWS_KEY') ?: $key,
                'secret' => getenv('AWS_SECRET') ?: $secret
            ],
            'region' => getenv('AWS_REGION') ?: $region,
            'version' => getenv('AWS_VERSION') ?: $version
        ]);
    }
}
