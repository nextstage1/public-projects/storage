<?php

namespace Storage\Adapters;

use DateTimeInterface;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnableToGenerateTemporaryUrl;
use League\Flysystem\Config;
use Storage\Contracts\AdapterInterface;
use Throwable;

class LocalAdapter extends LocalFilesystemAdapter implements AdapterInterface
{
    public function temporaryUrl(string $path, DateTimeInterface $expiresAt, Config $config): string
    {
        try {
            return (string) $path;
        } catch (Throwable $exception) {
            throw UnableToGenerateTemporaryUrl::dueToError($path, $exception);
        }
    }
}
