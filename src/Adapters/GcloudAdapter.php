<?php

namespace Storage\Adapters;

use Carbon\Carbon;
use League\Flysystem\Config;
use League\Flysystem\GoogleCloudStorage\GoogleCloudStorageAdapter;
use Storage\Contracts\AdapterInterface;

class GcloudAdapter extends GoogleCloudStorageAdapter implements AdapterInterface
{
}
