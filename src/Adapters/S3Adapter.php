<?php

namespace Storage\Adapters;

use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use Storage\Contracts\AdapterInterface;

class S3Adapter extends AwsS3V3Adapter implements AdapterInterface
{
}
