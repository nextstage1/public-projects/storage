<?php

namespace Storage;

use Exception;
use League\Flysystem\Filesystem;
use League\Flysystem\PathNormalizer;
use League\Flysystem\StorageAttributes;
use League\Flysystem\UnableToReadFile;
use League\Flysystem\UnableToWriteFile;
use League\Flysystem\UrlGeneration\PublicUrlGenerator;
use League\Flysystem\UrlGeneration\TemporaryUrlGenerator;
use Storage\Adapters\GcloudAdapter;
use Storage\Adapters\LocalAdapter;
use Storage\Adapters\S3Adapter;
use Storage\Clients\GcloudClient;
use Storage\Clients\LocalClient;
use Storage\Clients\S3Client;
use Storage\Contracts\AdapterInterface;

class Storage extends Filesystem
{
    private Filesystem $fs;
    private AdapterInterface $adapter;

    public static function make(string $adapterClassName, ?string $bucketName = null): Storage
    {
        switch ($adapterClassName) {
            case S3Adapter::class:
                $adapter = new S3Adapter(
                    S3Client::generate(),
                    $bucketName ?? getenv('AWS_BUCKET')
                );
                break;

            case GcloudAdapter::class:
                $adapter = new GcloudAdapter(
                    GcloudClient::generate()
                        ->bucket(
                            $bucketName ?? getenv('GCLOUD_BUCKET')
                        )
                );
                break;

            case LocalAdapter::class:
                $adapter = new LocalAdapter(
                    LocalClient::generate(
                        $bucketName ?? getenv('STORAGE_ROOT')
                    )
                );
                break;
            default:
                throw new Exception('Adapter not found');
        }

        return new Storage($adapter);
    }

    private function __construct(
        AdapterInterface $adapter,
        array $config = [],
        PathNormalizer $pathNormalizer = null,
        ?PublicUrlGenerator $publicUrlGenerator = null,
        ?TemporaryUrlGenerator $temporaryUrlGenerator = null,
    ) {
        parent::__construct($adapter, $config, $pathNormalizer, $publicUrlGenerator, $temporaryUrlGenerator);
        $this->adapter = $adapter;
    }

    public function getFs()
    {
        return $this->fs;
    }


    public function list(string $path = '', bool $deep = self::LIST_SHALLOW): array
    {
        return array_map(
            fn ($item) => $item['type'] === 'file'
                ? array_merge($item, [
                    'filename' => $item['path'] ?? '',
                    'timestamp' => $item['last_modified'] ?? '',
                    'size' => $item['file_size'] ?? '',
                    'storageclass' => $item['extra_metadata']['StorageClass'] ?? '',
                    'etag' => $item['extra_metadata']['ETag'] ?? '',
                ])
                : [
                    'dirname' => $item['path'] ?? '',
                    'basename' => $item['path'] ?? '',
                    'filename' => $item['path'] ?? '',
                ],
            parent::listContents($path, $deep)
                ->map(fn (StorageAttributes $item) => $item->jsonSerialize())
                ->toArray()
        );
    }

    /**
     * Save a local file on storage
     *
     * @param string $file Original file
     * @param string $path Path to save with ten name of file
     * @param boolean $overwrite
     * @return boolean
     */
    public function save(string $file, string $path, bool $overwrite = true): bool
    {
        if (!file_exists($file)) {
            throw new UnableToReadFile("File $file not found");
        }

        if (!$overwrite && parent::has($path)) {
            throw new UnableToWriteFile("File $path has be exists on storage");
        }

        parent::write($path, file_get_contents($file));

        return parent::has($path);
    }

    /**
     * Get a file from storage
     *
     * @param string $path Path on the storage
     * @param string $saveto Path to save, included name of file
     * @param boolean $overwrite
     * @return void
     */
    public function download(string $path, string $saveto, bool $overwrite = true)
    {
        // Load
        if (!parent::has($path)) {
            throw new UnableToReadFile("File $path not found on storage");
        }
        if (!$overwrite && file_exists($saveto)) {
            throw new UnableToWriteFile("File $saveto has be exists");
        }

        // Load
        $content = parent::read($path);

        $parts = explode(DIRECTORY_SEPARATOR, $saveto);
        $file = array_pop($parts);
        $directory = implode(DIRECTORY_SEPARATOR, $parts);
        if (strlen($directory) > 0 && !is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        // Save and return
        file_put_contents($saveto, $content);
        return file_exists($saveto);
    }
}
