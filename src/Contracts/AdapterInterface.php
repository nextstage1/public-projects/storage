<?php

namespace Storage\Contracts;

use DateTimeInterface;
use League\Flysystem\Config;
use League\Flysystem\FilesystemAdapter;

interface AdapterInterface extends FilesystemAdapter

{
    public function temporaryUrl(string $path, DateTimeInterface $expiresAt, Config $config): string;
}
