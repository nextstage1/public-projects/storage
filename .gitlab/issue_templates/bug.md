#### Situação esperada

#### Situação atual

#### Passo a passo para reproduzir o problema/erro

---------------------------------------------------------------------------------
### Somente para equipe que ira tratar o problema. Não alterar daqui para baixo.

#### Iniciação
- [ ] Entendimento da situação
- [ ] Viabilidade técnica, pré-requisitos
- [ ] Estimativa e prazos definidos

#### Planejamento
- [ ] Definir etiquetas
- [ ] Definir envolvidos
- [ ] Definir milestone
- [ ] Definir caso de uso
- [ ] Definir tarefas
- [ ] Definir esforço previsto
- [ ] Definir tarefas relacionadas
- [ ] Definir data de entrega prevista
- [ ] Alocar recursos
- [ ] Iniciar execução

#### Execução
- [ ] TAREFAS
- [ ] Validação de escopo
- [ ] Conclusão de tarefa

#### Gestão
- [ ] Comunicação aos interessados

#### Entrega
- [ ] Deploy em homologação
- [ ] Homologação desenvolvimento
- [ ] Deploy em release
- [ ] Homologação time de produto
- [ ] Documentações
